/*Copyright (c) 2017 Stoned Xander

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/
#ifndef SIMPLE_FRAMEWORK_SVG_BANK
#define SIMPLE_FRAMEWORK_SVG_BANK
#include <nanosvg/nanosvg.h>
#include <SFML/Graphics.hpp>
#include <map>

/**
 * An SVG bank allows storing parsed SVG images in order to
 * rasterize them in texture for further usage.
 */
class SVGBank {
   public:
    /**
     * Constructor.
     */
    SVGBank();

    /**
     * Destructor.
     */
    ~SVGBank();

    /**
     * Store a new SVG.
     * @param key Key for later retrieval.
     * @param path Path to the SVG file.
     * @return 'true' if everything went well.
     */
    bool store(const std::string &key, const std::string &path);

    /**
     * Rasterize the specified SVG to a texture.
     * @param key Key to the asset.
     * @param texture Target texture.
     * @param rect Position in the texture.
     * @return 'true' if the rasterisation is a success.
     * Basically, it returns 'false' when the key is invalid.
     */
    bool retrieve(const std::string &key, sf::Texture &texture,
                  sf::IntRect rect);

    /**
     * Remove (if possible) the SVG image stored
     * under the specified key.
     * @param key Storage key.
     */
    void remove(const std::string &key);

   private:
    /**
     * SVG storage.
     */
    std::map<std::string, ::NSVGimage *> data;
};

#endif
