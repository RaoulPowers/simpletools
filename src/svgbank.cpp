/*Copyright (c) 2017 Stoned Xander

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.*/
#define NANOSVG_IMPLEMENTATION
#include <svgbank.h>
#define NANOSVGRAST_IMPLEMENTATION
#include <nanosvg/nanosvgrast.h>
#include <algorithm>
#include <cmath>

SVGBank::SVGBank() {}

SVGBank::~SVGBank() {
    for (auto &i : data) {
        nsvgDelete(i.second);
    }
}

bool SVGBank::store(const std::string &key, const std::string &path) {
    remove(key);
    ::NSVGimage *image =
        ::nsvgParseFromFile(path.c_str(), "px", 96.0f);  // TODO Make parameters
    bool result;
    if (image != nullptr) {
        data[key] = image;
        result = true;
    } else {
        result = false;
    }
    return result;
}

void SVGBank::remove(const std::string &key) {
    const auto &it = data.find(key);
    if (it != data.end()) {
        nsvgDelete(data[key]);
        data.erase(it);
    }
}

bool SVGBank::retrieve(const std::string &key, sf::Texture &texture,
                       sf::IntRect rect) {
    bool result;
    const auto &it = data.find(key);
    if (it == data.end()) {
        result = false;
    } else {
        ::NSVGimage *image = data[key];
        double ratio = rect.width / image->width;
        sf::Vector2u offset(0, 0);
        if (rect.height < image->height * ratio) {
            ratio = rect.height / image->height;
            offset.x = (rect.width - ::ceil(image->width * ratio)) / 2;
        } else {
            offset.y = (rect.height - ::ceil(image->height * ratio)) / 2;
        }

        sf::Vector2u scaledSize(::ceil(image->width * ratio),
                                ::ceil(image->height * ratio));
        const unsigned int dataSize = scaledSize.x * scaledSize.y * 4;
        unsigned char *data = new unsigned char[dataSize];
        ::NSVGrasterizer *rasterizer = ::nsvgCreateRasterizer();
        ::nsvgRasterize(rasterizer, image, 0, 0, ratio, data, scaledSize.x,
                        scaledSize.y, scaledSize.x * 4);
        ::nsvgDeleteRasterizer(rasterizer);
        sf::Image patch;
        patch.create(scaledSize.x, scaledSize.y, data);
        delete[] data;
        texture.update(patch, rect.left + offset.x, rect.top + offset.y);
        result = true;
    }
    return result;
}
