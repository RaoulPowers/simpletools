# Simple Tools

## Compilation
```
mkdir build
cd build
cmake ..
make
```
Build the static library and all the examples.

## Requirements

* SFML (>2.4)

## Examples
* svgloader : An example of SVG storage, retrieval and rasterisation using [nanosvg](https://github.com/memononen/nanosvg).

## License

This library is licensed under zlib.
