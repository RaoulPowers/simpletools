/*Copyright (c) 2017 Stoned Xander

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.*/
#include <iostream>
#include <cstring>

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <svgbank.h>

#define WIDTH 1200
#define HEIGHT 800

/*
 * Main procedure.
 */
int main() {
    // Rendering window.
    sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "Simple SVG Loader",
                            sf::Style::Default, sf::ContextSettings(24));
    window.setVerticalSyncEnabled(true);

    sf::ContextSettings settings = window.getSettings();
    std::cout << "[??] OpenGL " << settings.majorVersion << "."
              << settings.minorVersion << std::endl;
    window.setActive(true);

    // Retrieve SVG assets ---------------------------------------------------
    sf::Texture texture;
    texture.create(WIDTH, HEIGHT);
    // texture is initialize to total black.
    unsigned char*data = new unsigned char[WIDTH*HEIGHT*4];
    ::memset(data, 255, WIDTH*HEIGHT*4);
    texture.update(data);
    delete[]data;
    // Set the sprite tha will display the texture.
    sf::Sprite sprite;
    sprite.setPosition({0, 0});
    sprite.setTexture(texture);

    // Initialize the bank.
    SVGBank bank;

    // Pre-load SVG.
    bank.store("tiger", "res/Ghostscript_Tiger.svg");
    bank.store("mememe", "res/mememe.svg");
    bank.store("mozilla", "res/mozilla.svg");

    // Retrieve SVG to a texture.
    bank.retrieve("tiger", texture, {0, 0, WIDTH-1, HEIGHT-1});
    bank.retrieve("tiger", texture, {0, 0, 100, 100});
    bank.retrieve("mememe", texture, {500, 50, 128, 128});
    bank.retrieve("mozilla", texture, {300, 300, 200, 100});

    // Just a simple shape to show in which space the mozilla logo
    // was meant to be rasterized.
    sf::RectangleShape shape;
    shape.setOutlineColor(sf::Color::Red);
    shape.setFillColor(sf::Color::Transparent);
    shape.setOutlineThickness(3);
    shape.setPosition({300, 300});
    shape.setSize({200, 100});

    // -- Main Loop
    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) window.close();
            if (event.type == sf::Event::KeyPressed) {
                switch (event.key.code) {
                    case sf::Keyboard::Escape:
                        window.close();
                        break;
                    default:
                        break;
                }
            }
        }
        window.clear(sf::Color::Blue);

        window.draw(sprite);
        window.draw(shape);

        window.display();
    }

    return 0;
}
